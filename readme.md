### Sprint Meetings

![datei](/blob/scrum.png)

(Por: https://www.visual-paradigm.com/servlet/editor-content/scrum/what-is-sprint-review/sites/7/2018/11/sprint-review-vs-sprint-retrospective.png)

### Epics / Histórias / Tarefas

![datei](/blob/epic.png)

(Por: https://k21.global/blog/product-backlog-epico-historia-tarefas)

### Product Owner(P.O.)

  ![datei](/blob/po.jpg)

  (Por: https://developerexperience.io/practices/product-owner)

  ### O que é?
    - Representa a comunicação entre os stakeholders e o time.
    - Tem total controle sobre o que as manutenções realizadas no software
  ### O que faz?
    - Responsável por manter o backlog 
    - Define histórias e prioridades

### Scrum Master

  ![datei](/blob/sm.jpg)

  (Por: https://www.mundodeportivo.com/r/GODO/MD/p2/MasQueDeporte/Imagenes/2014/03/04/Recortada/MD_20140304_FOTOS_D_54402010695-652x492@MundoDeportivo-Web.jpg)

  ### O que é?
    - Mestre dessa estrutura, que garante que ela seja seguida.
  ### O que faz?
    - Facilita o trabalho e a comunicação do time.
    - Auxilia o P.O. na gestão e priorização do Backlog.
    - Remove os impedimentos reportados pelo time.

### Vantagens

  - Foco
  - Feedback contínuo
  - Adaptação rápida, não fácil
  - Melhoria contínua*
  - Motivação do time*

### Eventos

  - Para fazer uma sprint, além do momento da execução, há 4 eventos sagrados: 
    - Planejamento (Sprint Planning) 
    - Revisão (Review Meeting)
    - Retrospectiva (Retrospective Meeting)
    - Diária (Daily Meeting)
  - São eventos extremamente importantes para o time, sendo imprescindível a presença de todos os membros e seu total comprometimento com esses eventos.

### Atenção!

  ![datei](/blob/Implementation1.png)

  (Por Caique Rebouças)

  A fase de implementação de uma sprint deve estar blindada de qualquer tarefa não planejada que não seja de consentimento do time.

  ![datei](/blob/Implementation2.png)

  (Por Caique Rebouças)

### Abertura da sprint

  ![datei](/blob/Sprint-Planning.jpg)

  (Por: https://agilebox.com.br/wp-content/uploads/2018/10/SprintPlanning.jpg)

  ### Refinamento das histórias    

    ### Quem deve participar?
      - Time + Stakeholders + P.O.
    ### O que fazer?
      - Ajustar detalhes relacionados às regras de negócio das histórias
      - Definição da sprint
      - Bugs sempre no topo do quadro
      - Seguido pelas tarefas remanescentes da sprint anterior
      - Versionamento das tarefas
      - Novas tarefas que entram no meio da sprint devem substituir outra, mediante negociação com o time.

  ### Estimando as tarefas (Planning Poker)

  ![datei](/blob/Planning-Poker.png)

  (Por: https://agilepink.com/planning-poker/2019/)

  ### Quem deve participar?
  - Time(programadores)
  ### O que fazer?
  - Avaliar e pontuar a complexidade das tarefas.
  - Quanto mais difícil a tarefa, maior a pontuação.
  - O membro com a menor e o maior número devem se justificar aos demais para entendimento e convergência da pontuação.
  - Fragmentar a história em pequenas tarefas
  - Porque bugs não são pontuados:
      - Bugs representam regressão, não evolução do software.
      - Atrapalham a métrica do software.

### Reunião diária (Daily Meeting) 

  ![datei](/blob/Burndown1.png)

  (Por: https://elearningindustry.com/charts-in-agile-development-8-components-uses-burndown)

  ### Quem deve participar?
    - Time
  ### O que fazer
    - Cada membro do time responde as seguintes questões:
    - O que você fez ontem?
    - O que irá fazer hoje?
    - Teve algum impedimento(obstáculos externos, ou seja, precisam ser resolvidos por pessoas de fora do time)?
    - Reportar impedimentos no “log de impedimentos” 
    - Apresentar o desempenho do time até o momento
    - Quantos pontos foram feitos?
    - Quanto pontos faltam?

### Fechamento da sprint

  ![datei](/blob/revisao.png)

  (Por Caique Rebouças)

  ![datei](/blob/Burndown2.png)

  (Por: https://78462f86-a-668b4887-s-sites.googlegroups.com/a/effectivepmc.com/www/blog/agile/information-radiators/burn-down-chart/ReleaseBurnDown.png?attachauth=ANoY7coD0tDEsQm_YgDbY-PejI9-igUow2iTGBC0hFp-xXngO0XlDrmWeRVDtdVe1rsF1atAI2T4068dugRVlRZtTA8qD-sEcEJuYNPgpKP-F9gH8WBTaNOQSS5hGsL1oFtmvgefSss4Qz8_DgEGj5ZHc-yvYnCwqbnsFmfm0xPE6OIgneuXVIWQdSVcvp_UxahoR6---9ePhiaVb03TXevaFCcxiIgerg_-6KMLaDbrO5iGtEnqshH6U3YMkZ9bUViARtM3SXxlqi99Zjk6zKa2wLdtBwbjs6udavqEOVPYFD-vnsclMec%3D&attredirects=0)


  ### Revisão da sprint (Sprint Review)
  
    ### Quem deve participar?
      - P.O. + Time + Stakeholders 
    ### O que fazer?
      - Apresentar o funcionamento de todas as tarefas concluídas aos stakeholders
      - Apresentar desempenho do time na sprint e geral

  ### Retrospectiva (Sprint Retrospective)

    - A parte mais simples e rápida, mas não menos importante.

    ### Quem participa?
      - Time
    ### O que fazer?
      - O time responde TODAS as perguntas(reavaliando a resposta da sprint anterior):
        - O que correu bem no Sprint?
        - O que poderia ser melhorado?
        - O que vamos nos comprometer a melhorar no próximo Sprint?
        - Comentários adicionais.

### Roteiro

  → Fechamento da Sprint (Revisão → Retrospectiva) 
  → Abertura da sprint (Definição da sprint → Estimando as tarefas)







